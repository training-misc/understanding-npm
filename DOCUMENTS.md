# Node packages

### Semantic Versioning

5.21.17\
5 -> Major (Big Changes that affects the current version or new features)\
21 -> Minor (Updates some functionality but did not affect the current version)\
17 -> Patches (Bug fixes, Small changes)

### Versions
|             |            |          
|-------------|------------|
|Exact version | 5.21.17|
|Greater than | >5.21.1 |
| Compatible changes | ^5.21.8 |
| Minor-level changes | ~5.21.8|\



### Depedencies
- Installed using npm install <packagename>
- If a package is installed using npm install without the devDependencies tag, then all that package dependencies will install its dependencies along with it
- Transitive Installation

### Development Dependencies
- Installed using npm install --save-dev or -D or --save
- If a package is installed and it has devDependencis, then that dependecies will not be installed


### Browser App
- Requires HTML, CSS and Etc

### Server Package
- Don't have HTML and only have JS files

### Add Dependencies Only If
- Your package is public
- Compiled version of your package uses features from dependent packages
- Other packages depend on your package

### Exploring Package Information and Version
`npm view <package>` to view the package information \
`npm view <package> version` to view the list of package version

### Install Specific Package Version
`npm install <package>@version` to install the specific version

### Why package-lock.json file is needed?
It keeps version tree of the project dependences including child dependencies

### Lock File Summary
- Guarantees consistency of the dependencies versions
- Generate and updated automatically
- Committed to source control

### Update NPM Packages
`npm update` it updates all listed package to the latest release version.\
For example `^1.2.5 -> ^1.3.0`.

### List of NPM Built-in commands for scripts
- `npm start`
- `npm stop`
- `npm restart`
- `npm test`
- `npm prestart`
- `npm poststart`

### How to run Several Scripts in parallel
- `npm i -D npm-run-all`
- `npm-run-all --parallel custom1 custom 2`

### Executables in the .BIN folder
- During npm package installation bin scripts are copied to the .bin folder

```json
"bin" {
    "run-p": "bin/run-p/index.js",
    "run-s": "bin/run-s/index.js",
    "npm-run-all": "bin/npm-run-all/index.js"
}
```

### Shebang
Determines which interpreter shoudl be use for file execution on the Unix-like Operating Suystem

